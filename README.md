# podcast_downloader

`podcast_downloader` is a podcast downloader (wrapping `greg`) used by
`cyclesbot` that manages downloaded podcasts and generates an RSS file.
