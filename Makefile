.PHONY: clean build test deploy


clean:
	sudo rm -rf podcast_downloader/__pycache__


build: clean
	docker build -t registry.tompaton.com/podcast_downloader .


test:
	docker run -it --rm \
	  -v `pwd`:/src \
	  --entrypoint "python" \
	  registry.tompaton.com/podcast_downloader \
	  -m pytest -vv /src/podcast_downloader/

deploy: build
	docker push registry.tompaton.com/podcast_downloader
	ssh tom@magnesium "docker pull registry.tompaton.com/podcast_downloader"
	rcp remote-docker-run.sh magnesium:/var/data/tom/podcasts/docker-run.sh
