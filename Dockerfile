FROM python:3.6

RUN apt-get update && apt-get install -y sox

RUN mkdir -p /src /podcasts/downloaded /podcasts/processed /podcasts/archived \
             /podcasts/deleted /podcasts/progress /podcasts/images

COPY podcast_downloader /src/podcast_downloader
COPY LICENSE README.md setup.py /src/

WORKDIR /src
RUN python setup.py build

RUN pip install --no-cache-dir -e /src/
#RUN pip install --no-cache-dir /src/

WORKDIR /podcasts

ENTRYPOINT ["python", "-m", "podcast_downloader"]
