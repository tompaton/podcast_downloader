import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="podcast_downloader",
    version="1.1.0",
    author="Tom Paton",
    author_email="tom.paton@gmail.com",
    description="podcast_downloader module for cyclesbot",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.com/tompaton/podcast_downloader",
    packages=setuptools.find_packages(),
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ),
    install_requires=[
        'pytest==4.4.0',
        'mutagen==1.39',
        'Greg==0.4.7',
        'stagger==1.0.0',
    ],
)
