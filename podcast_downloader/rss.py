import email.utils
import hashlib
import mutagen
from datetime import datetime
from pathlib import Path


_RSS = '''
<?xml version="1.0" encoding="ISO-8859-1"?>
<rss xmlns:itunes="http://www.itunes.com/DTDs/Podcast-1.0.dtd" version="2.0">
    <channel>
      <title>Downloaded - podcasts.tompaton.com</title>
      <description>Downloaded podcasts (post-processed to increase tempo,
      convert to 64kbit mono).</description>
      <link>https://podcasts.tompaton.com</link>
      <lastBuildDate>{timestamp}</lastBuildDate>
      <generator>Ha!</generator>
      <language>en_US.UTF-8</language>
      <ttl>1800</ttl>
      {entries}
    </channel>
</rss>
'''.strip()


def make_rss(mp3s, processed):
    entries = [format_entry(get_entry(mp3, processed / mp3.name))
               for mp3 in reversed(mp3s)]
    return _RSS.format(timestamp=email.utils.format_datetime(datetime.now()),
                       entries='\n'.join(entries))


def get_entry(path, path2):
    entry = {'guid': path.name,
             'author': 'Unknown author',
             'title': 'Unknown title',
             'url': 'https://podcasts.tompaton.com/downloaded/' + path.name,
             'filename': str(path2.resolve()),
             'duration': '',  # 1:20:28
             'duration_seconds': 0,
             'type': '',  # audio/mpeg
             'description': '',
             'image': None}

    mtime = datetime.fromtimestamp(path.stat().st_mtime)
    entry['pubDate'] = email.utils.format_datetime(mtime)

    if path2.exists():
        entry['length'] = path2.stat().st_size
    else:
        entry['length'] = path.stat().st_size

    try:
        tags = mutagen.File(path.open('rb'), easy=True)
    except:
        tags = None

    if tags:
        keys = set(tags.keys())
        ignored_tags = {'genre', 'copyright', 'tracknumber', 'bpm',
                        'date', 'language', 'encodedby', 'length', 'media',
                        'artistsort', 'albumsort', 'composersort', 'titlesort'}

        author = get_value(tags, keys, ['organization', 'composer',
                                        'albumartist', 'artist', 'author'])
        if author:
            entry['author'] = author

        title = get_value(tags, keys, ['album', 'title'])
        if title:
            entry['title'] = title

        entry['description'] = get_value(tags, keys, ['version'])

        remaining = {key: tags[key] for key in (keys - ignored_tags)}
        if remaining:
            print('** UNHANDLED TAGS ** ' + repr(remaining))

        if tags.mime:
            entry['type'] = tags.mime[0]

        # extract "cover" image
        image = extract_image(path)
        if image:
            entry['image'] = 'https://podcasts.tompaton.com/' + image

        # size & duration from processed file
        if path2.exists():
            try:
                tags2 = mutagen.File(path2.open('rb'), easy=True)
            except:
                tags2 = None

            if tags2 is not None:
                s = int(tags2.info.length)
                entry['duration_seconds'] = s
                entry['duration'] = '{}:{:02}:{:02}'.format(s // 3600,
                                                            s % 3600 // 60,
                                                            s % 60)

    return entry


def get_value(tags, remaining_keys, keys):
    values = []

    for key in keys:
        if key in tags:
            value = cleanup_value(key, tags[key][0])
            if value and value not in values:
                values.append(value)
            remaining_keys.remove(key)

    return ' '.join(values)


_OVERRIDES = {
    'organization':
    {
        # redundant
        'Radio National (Australian Broadcasting Corporation)': None,

        # don't care
        'Science Friday Initiative': None,
    },
    'albumartist':
    {
        # don't care
        'Chicago Public Media': None,
    },
    'artist':
    {
        # only value in author, so can't remove entirely
        'Canadian Broadcasting Corporation': 'CBC',
    }
}


def cleanup_value(key, value):
    if key in _OVERRIDES:
        return _OVERRIDES[key].get(value, value)

    return value


def extract_image(path):
    try:
        tags = mutagen.File(path.open('rb'))
    except:
        pass
    else:
        for tag in tags:
            if tag.startswith('APIC'):
                data = tags.tags[tag].data
                img_hash = hashlib.md5()
                img_hash.update(data)
                fn = 'images/{}'.format(img_hash.hexdigest())
                open('/podcasts/{}'.format(fn), 'wb').write(data)
                return fn


def format_entry(entry):
    tags = ['<author>{}</author>'.format(escape(entry['author'])),
            '<title>{}</title>'.format(escape(entry['title'])),
            '<pubDate>{}</pubDate>'.format(entry['pubDate']),
            '<enclosure url="{}" length="{}" type="{}"/>'
            .format(entry['url'], entry['length'], entry['type']),
            '<guid>{}</guid>'.format(entry['guid'])]

    if entry['description']:
        tags.append('<description><![CDATA[{}]]></description>'
                    .format(entry['description']))

    if entry['duration']:
        tags.append('<itunes:duration>{}</itunes:duration>'
                    .format(entry['duration']))

    if entry['image']:
        tags.append('<itunes:image href="{}" />'
                    .format(entry['image']))

    return ('<item>\n        {}\n      </item>'
            .format('\n        '.join(tag for tag in tags)))


def escape(s):
    return (s.replace("&", "&amp;")
            .replace("<", "&lt;")
            .replace(">", "&gt;")
            .replace("\"", "&quot;"))
