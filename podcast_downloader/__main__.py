import argparse
from . import manage


def list_from_args(args):
    print(manage.list_downloads(grep=args.name))


def rss_from_args(args):
    manage.generate_rss()
    print('Generated downloaded.rss')


def remove_from_args(args):
    print(manage.remove_download(mp3s=args.name))
    manage.generate_rss()


def undo_from_args(args):
    print(manage.undo_remove())
    manage.generate_rss()


def save_from_args(args):
    print(manage.save_download(mp3s=args.name))
    manage.generate_rss()


def play_from_args(args):
    pass


def download_from_args(args):
    print(manage.download_episode(urls=args.name))
    manage.generate_rss()



# TODO: sync
# TODO: make
# TODO: subscribe


parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers(dest='func_name')

parser_list = subparsers.add_parser('list', aliases=['ls'])
parser_list.set_defaults(func=list_from_args)
parser_list.add_argument('name', nargs='*', default=[])

parser_rss = subparsers.add_parser('rss')
parser_rss.set_defaults(func=rss_from_args)

parser_remove = subparsers.add_parser('remove', aliases=['rm', 'delete'])
parser_remove.set_defaults(func=remove_from_args)
parser_remove.add_argument('name', nargs='*', default=[])

parser_undo = subparsers.add_parser('undo')
parser_undo.set_defaults(func=undo_from_args)

parser_save = subparsers.add_parser('save', aliases=['archive'])
parser_save.set_defaults(func=save_from_args)
parser_save.add_argument('name', nargs='+')

parser_play = subparsers.add_parser('play')
parser_play.set_defaults(func=play_from_args)
parser_play.add_argument('name', nargs='+')

parser_download = subparsers.add_parser('download')
parser_download.set_defaults(func=download_from_args)
parser_download.add_argument('name', nargs='+')


if __name__ == '__main__':
    args = parser.parse_args()
    if args.func_name:
        args.func(args)
    else:
        parser.print_help()
