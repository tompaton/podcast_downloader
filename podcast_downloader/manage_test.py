import pytest
from unittest.mock import patch, Mock, call, mock_open
from podcast_downloader import manage


def test_list_empty():
    assert manage.list_downloads() == '*Downloaded files:*\n'


@pytest.fixture
def downloaded_files():
    file1 = Mock() ; file2 = Mock() ; file3 = Mock()
    file1.name = 'show1_ep1.mp3'
    file2.name = 'show1_ep2.mp3'
    file3.name = 'show2_ep1.mp3'
    return [file3, file2, file1]


def test_list(downloaded_files):
    with patch('podcast_downloader.manage._downloaded') as downloaded:
        downloaded.return_value = downloaded_files

        assert manage.list_downloads() == ('*Downloaded files:*\n'
                                           '1 show2\\_ep1.mp3\n'
                                           '2 show1\\_ep2.mp3\n'
                                           '3 show1\\_ep1.mp3')


def test_downloaded():
    with patch.object(manage, 'Path') as path:
        path.return_value = downloaded = Mock()

        file1 = Mock(stat=Mock(return_value=Mock(st_mtime=3)))
        file2 = Mock(stat=Mock(return_value=Mock(st_mtime=2)))
        file3 = Mock(stat=Mock(return_value=Mock(st_mtime=1)))

        downloaded.iterdir.return_value = iter([file1, file2, file3])

        assert manage._downloaded() == [file3, file2, file1]

        assert path.mock_calls == [call('/podcasts/downloaded'),
                                   call().iterdir()]


def test_list_grep_empty():
    assert manage.list_downloads(['x']) == '*Downloaded files:*\n'


def test_list_grep_none(downloaded_files):
    with patch('podcast_downloader.manage._downloaded') as downloaded:
        downloaded.return_value = downloaded_files

        assert manage.list_downloads(['x']) == '*Downloaded files:*\n'


def test_list_grep(downloaded_files):
    with patch('podcast_downloader.manage._downloaded') as downloaded:
        downloaded.return_value = downloaded_files

        assert manage.list_downloads(['ep2']) == ('*Downloaded files:*\n'
                                                  '2 show1\\_ep2.mp3')


def test_list_grep_match(downloaded_files):
    with patch('podcast_downloader.manage._downloaded') as downloaded:
        downloaded.return_value = downloaded_files

        assert manage.list_downloads(['ep2', 'show']) \
            == ('*Downloaded files:*\n'
                '2 show1\\_ep2.mp3')


def test_list_grep_no_match(downloaded_files):
    with patch('podcast_downloader.manage._downloaded') as downloaded:
        downloaded.return_value = downloaded_files

        assert manage.list_downloads(['ep2', 'x']) == '*Downloaded files:*\n'


def test_rss(downloaded_files):
    with patch.object(manage, 'Path') as path, \
         patch('builtins.open', mock_open()) as output, \
         patch.object(manage.rss, 'make_rss') as make_rss, \
         patch('podcast_downloader.manage._downloaded') as downloaded:
        processed = Mock()
        path.side_effect = [Mock(), processed]
        downloaded.return_value = downloaded_files
        make_rss.return_value = 'rss xml'

        manage.generate_rss()

        assert path.mock_calls == [call('/podcasts/downloaded'),
                                   call('/podcasts/processed')]
        output.assert_called_with('/podcasts/downloaded.rss', 'w')
        output().write.assert_called_with('rss xml')
        assert make_rss.mock_calls == [call(downloaded_files, processed)]


def test_completed_none():
    assert manage._completed() == []


@pytest.mark.parametrize('data,result', [
    ('', []),
    ('show1_ep2.mp3 show2_ep1.mp3', ['show1_ep2.mp3', 'show2_ep1.mp3']),
])
def test_completed(data, result):
    with patch.object(manage, 'Path') as path:
        mock_path = Mock()
        path.side_effect = [mock_path]
        mock_path.open = mock_file = Mock()
        mock_file.side_effect = mock_open(read_data=data)

        assert manage._completed() == result

        assert path.mock_calls == [call('/podcasts/progress/completed.txt')]
        assert mock_path.mock_calls == [call.exists(), call.open()]


def test_remove_download_not_found():
    assert manage.remove_download(['show1_ep2.mp3']) \
        == '*Not found!*\nshow1\\_ep2.mp3'


def test_remove_download_completed_empty_none():
    assert manage.remove_download() == ''


def test_remove_download_completed_empty():
    assert manage.remove_download([]) == ''


def test_remove_download_completed():
    with patch('podcast_downloader.manage._completed') as completed:
        completed.return_value = ['show1_ep2.mp3', 'show2_ep1.mp3']
        assert manage.remove_download() \
            == '*Not found!*\nshow1\\_ep2.mp3\nshow2\\_ep1.mp3'


def test_remove_download_not_exists():
    with patch.object(manage, 'Path') as path:
        processed = Mock()
        downloaded = Mock()
        archived = Mock()
        deleted = Mock()
        path.side_effect = [processed, downloaded, archived, deleted]

        processed.joinpath().exists.return_value = False
        archived.joinpath().exists.return_value = False

        assert manage._remove_download('fn.mp3') == ('Not found!', 'fn.mp3')

        assert path.mock_calls == [call('/podcasts/processed'),
                                   call('/podcasts/downloaded'),
                                   call('/podcasts/archived'),
                                   call('/podcasts/deleted')]
        assert processed.joinpath.mock_calls == [
            call(),  # <-- from return_value setup above
            call('fn.mp3'), call().exists()]
        assert downloaded.joinpath.mock_calls == [call('fn.mp3')]
        assert archived.joinpath.mock_calls == [
            call(),  # <-- from return_value setup above
            call('fn.mp3'), call().exists()]
        assert deleted.joinpath.mock_calls == [call('fn.mp3')]


def test_remove_download_processed():
    with patch.object(manage, 'Path') as path:
        processed = Mock()
        downloaded = Mock()
        archived = Mock()
        deleted = Mock()
        path.side_effect = [processed, downloaded, archived, deleted]

        assert manage._remove_download('fn.mp3') \
            == ('Removed download(s)', 'fn.mp3')

        assert path.mock_calls == [call('/podcasts/processed'),
                                   call('/podcasts/downloaded'),
                                   call('/podcasts/archived'),
                                   call('/podcasts/deleted')]
        assert processed.joinpath.mock_calls == [
            call('fn.mp3'), call().exists(), call().unlink()]
        assert downloaded.joinpath.mock_calls == [
            call('fn.mp3'), call().rename(deleted.joinpath.return_value)]
        assert archived.joinpath.mock_calls == [call('fn.mp3')]
        assert deleted.joinpath.mock_calls == [call('fn.mp3'), call().touch()]


def test_remove_download_archived():
    with patch.object(manage, 'Path') as path:
        processed = Mock()
        downloaded = Mock()
        archived = Mock()
        deleted = Mock()
        path.side_effect = [processed, downloaded, archived, deleted]

        processed.joinpath().exists.return_value = False

        assert manage._remove_download('fn.mp3') \
            == ('Removed from archive!', 'fn.mp3')

        assert path.mock_calls == [call('/podcasts/processed'),
                                   call('/podcasts/downloaded'),
                                   call('/podcasts/archived'),
                                   call('/podcasts/deleted')]
        assert processed.joinpath.mock_calls == [
            call(),  # <-- from return_value setup above
            call('fn.mp3'), call().exists()]
        assert downloaded.joinpath.mock_calls == [call('fn.mp3')]
        assert archived.joinpath.mock_calls == [
            call('fn.mp3'), call().exists(),
            call().rename(deleted.joinpath.return_value)]
        assert deleted.joinpath.mock_calls == [call('fn.mp3'), call().touch()]


@pytest.mark.parametrize('func,mp3s,result', [
    (lambda x: x, [], ''),
    (lambda x: (x[0], x), ['A1', 'A2', 'B3'], '*A*\nA1\nA2\n*B*\nB3'),
])
def test_results(func, mp3s, result):
    assert manage._results(func, mp3s) == result


def test_undo_none():
    assert manage.undo_remove() == 'Restored 0 deleted downloads.'


def test_undo():
    with patch.object(manage, 'Path') as path:
        deleted = Mock()
        downloaded = Mock()
        path.side_effect = [deleted, downloaded, downloaded]

        file1 = Mock() ; file2 = Mock()
        file1.name = 'show1_ep2.mp3'
        file2.name = 'show2_ep1.mp3'

        deleted.glob.return_value = [file1, file2]

        downloaded.joinpath.side_effect \
            = ['/podcasts/downloaded/show1_ep2.mp3',
               '/podcasts/downloaded/show2_ep1.mp3']

        assert manage.undo_remove() == 'Restored 2 deleted downloads.'

        assert path.mock_calls == [call('/podcasts/deleted'),
                                   call('/podcasts/downloaded'),
                                   call('/podcasts/downloaded')]

        assert deleted.mock_calls == [call.glob('*')]
        assert downloaded.mock_calls == [call.joinpath('show1_ep2.mp3'),
                                         call.joinpath('show2_ep1.mp3')]
        assert file1.mock_calls \
            == [call.rename('/podcasts/downloaded/show1_ep2.mp3')]
        assert file2.mock_calls \
            == [call.rename('/podcasts/downloaded/show2_ep1.mp3')]


def test_save_download_not_found():
    assert manage.save_download(['show1_ep2.mp3']) \
        == '*Not found!*\nshow1\\_ep2.mp3'


def test_save_download_not_exists():
    with patch.object(manage, 'Path') as path:
        processed = Mock()
        downloaded = Mock()
        archived = Mock()
        deleted = Mock()
        path.side_effect = [processed, downloaded, archived, deleted]

        processed.joinpath().exists.return_value = False
        deleted.joinpath().exists.return_value = False

        assert manage._save_download('fn.mp3') == ('Not found!', 'fn.mp3')

        assert path.mock_calls == [call('/podcasts/processed'),
                                   call('/podcasts/downloaded'),
                                   call('/podcasts/archived'),
                                   call('/podcasts/deleted')]
        assert processed.joinpath.mock_calls == [
            call(),  # <-- from return_value setup above
            call('fn.mp3'), call().exists()]
        assert downloaded.joinpath.mock_calls == [call('fn.mp3')]
        assert archived.joinpath.mock_calls == [call('fn.mp3')]
        assert deleted.joinpath.mock_calls == [
            call(),  # <-- from return_value setup above
            call('fn.mp3'), call().exists()]


def test_save_download_processed():
    with patch.object(manage, 'Path') as path:
        processed = Mock()
        downloaded = Mock()
        archived = Mock()
        deleted = Mock()
        path.side_effect = [processed, downloaded, archived, deleted]

        assert manage._save_download('fn.mp3') \
            == ('Archived download(s)', 'fn.mp3')

        assert path.mock_calls == [call('/podcasts/processed'),
                                   call('/podcasts/downloaded'),
                                   call('/podcasts/archived'),
                                   call('/podcasts/deleted')]
        assert processed.joinpath.mock_calls == [
            call('fn.mp3'), call().exists(), call().unlink()]
        assert downloaded.joinpath.mock_calls == [
            call('fn.mp3'), call().rename(archived.joinpath.return_value)]
        assert archived.joinpath.mock_calls == [call('fn.mp3')]
        assert deleted.joinpath.mock_calls == [call('fn.mp3')]


def test_save_download_deleted():
    with patch.object(manage, 'Path') as path:
        processed = Mock()
        downloaded = Mock()
        archived = Mock()
        deleted = Mock()
        path.side_effect = [processed, downloaded, archived, deleted]

        processed.joinpath().exists.return_value = False

        assert manage._save_download('fn.mp3') \
            == ('Restored from deleted!', 'fn.mp3')

        assert path.mock_calls == [call('/podcasts/processed'),
                                   call('/podcasts/downloaded'),
                                   call('/podcasts/archived'),
                                   call('/podcasts/deleted')]
        assert processed.joinpath.mock_calls == [
            call(),  # <-- from return_value setup above
            call('fn.mp3'), call().exists()]
        assert downloaded.joinpath.mock_calls == [call('fn.mp3')]
        assert archived.joinpath.mock_calls == [call('fn.mp3')]
        assert deleted.joinpath.mock_calls == [
            call('fn.mp3'), call().exists(),
            call().rename(archived.joinpath.return_value)]


def test_play_download():
    with patch.object(manage, 'Path') as path, \
         patch.object(manage.rss, 'get_entry') as get_entry, \
         patch('podcast_downloader.manage._downloaded') as downloaded:
        mp3 = Mock()
        mp3.name = 'fn.mp3'
        downloaded.return_value = [mp3]

        processed = Mock()
        downloaded = Mock()
        path.side_effect = [downloaded, processed]

        entry = {'filename': '/podcasts/processed/fn.mp3'}
        get_entry.return_value = entry

        assert manage.play_download('1') == entry

        assert path.mock_calls == [call('/podcasts/downloaded'),
                                   call('/podcasts/processed')]

        assert downloaded.joinpath.mock_calls == [call('fn.mp3')]
        assert processed.joinpath.mock_calls == [call('fn.mp3'),
                                                 call().exists()]
        assert get_entry.mock_calls == [call(downloaded.joinpath.return_value,
                                             processed.joinpath.return_value)]


def test_download_episode():
    with patch.object(manage.os, 'system') as mock_system:
        assert manage.download_episode(
            ['http://example.com/media/the%20episode+1.mp3']) \
            == 'Downloaded.'

    assert mock_system.mock_calls \
        == [call('wget --quiet -P /podcasts/downloaded/ '
                 '"http://example.com/media/the%20episode+1.mp3"')]


@pytest.mark.parametrize('mp3s,result', [
    ([], []),
    (['show1_ep2.mp3'], ['show1_ep2.mp3']),
    (['show1_ep2.mp3', 'show2_ep1.mp3'], ['show1_ep2.mp3', 'show2_ep1.mp3']),
    (['1'], ['show2_ep1.mp3']),
    (['1', '2'], ['show2_ep1.mp3', 'show1_ep2.mp3']),
    (['show1_ep1.mp3', '2'], ['show1_ep1.mp3', 'show1_ep2.mp3']),
])
def test_lookup_mp3s(mp3s, result, downloaded_files):
    with patch('podcast_downloader.manage._downloaded') as downloaded:
        downloaded.return_value = downloaded_files
        assert manage.lookup_mp3s(mp3s) == result
