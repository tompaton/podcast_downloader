import os, shlex
from operator import itemgetter
from itertools import groupby
from pathlib import Path

from podcast_downloader import rss


def list_downloads(grep=None):
    return '*Downloaded files:*\n{}'.format('\n'.join(
        '{} {}'.format(i + 1, escape_markdown(fn.name))
        for i, fn in enumerate(_downloaded())
        if not grep or all(word.lower() in fn.name.lower()
                           for word in grep)
    ))


def _downloaded():
    return sorted(Path('/podcasts/downloaded').iterdir(),
                  key=lambda fn: fn.stat().st_mtime)


def generate_rss():
    downloaded = Path('/podcasts/downloaded')
    processed = Path('/podcasts/processed')

    with open('/podcasts/downloaded.rss', 'w') as downloaded_rss:
        downloaded_rss.write(rss.make_rss(_downloaded(), processed))


def _completed():
    completed = Path('/podcasts/progress/completed.txt')
    if completed.exists():
        return completed.open().read().split()
    else:
        return []


def remove_download(mp3s=None):
    if mp3s:
        mp3s = lookup_mp3s(mp3s)
    else:
        mp3s = _completed()

    return _results(_remove_download, mp3s)


def _results(func, mp3s):
    results = groupby((func(mp3_) for mp3_ in mp3s), key=itemgetter(0))
    return '\n'.join('*{}*\n{}'.format(title, '\n'.join(escape_markdown(fn)
                                                        for g, fn in fns))
                     for title, fns in results)


def _remove_download(fn):
    processed = Path('/podcasts/processed').joinpath(fn)
    downloaded = Path('/podcasts/downloaded').joinpath(fn)
    archived = Path('/podcasts/archived').joinpath(fn)
    deleted = Path('/podcasts/deleted').joinpath(fn)

    if processed.exists():
        processed.unlink()
        downloaded.rename(deleted)
        deleted.touch()
        return 'Removed download(s)', fn

    elif archived.exists():
        archived.rename(deleted)
        deleted.touch()
        return 'Removed from archive!', fn

    else:
        return 'Not found!', fn


def undo_remove():
    deleted = list(Path('/podcasts/deleted').glob('*'))
    for f in deleted:
        f.rename(Path('/podcasts/downloaded').joinpath(f.name))
    return 'Restored {} deleted downloads.'.format(len(deleted))


def save_download(mp3s):
    return _results(_save_download, lookup_mp3s(mp3s))


def _save_download(fn):
    processed = Path('/podcasts/processed').joinpath(fn)
    downloaded = Path('/podcasts/downloaded').joinpath(fn)
    archived = Path('/podcasts/archived').joinpath(fn)
    deleted = Path('/podcasts/deleted').joinpath(fn)

    if processed.exists():
        processed.unlink()
        downloaded.rename(archived)
        return 'Archived download(s)', fn

    elif deleted.exists():
        deleted.rename(archived)
        return 'Restored from deleted!', fn

    else:
        return 'Not found!', fn


def play_download(mp3s):
    fn = lookup_mp3s(mp3s)[0]

    downloaded = Path('/podcasts/downloaded').joinpath(fn)
    processed = Path('/podcasts/processed').joinpath(fn)

    if processed.exists():
        return rss.get_entry(downloaded, processed)

    else:
        return None


def download_episode(urls):
    for url in urls:
        safe_url = shlex.quote(url)
        os.system(f'wget --quiet -P /podcasts/downloaded/ "{safe_url}"')
        # TODO: make
    return 'Downloaded.'


def lookup_mp3s(mp3s):
    # TODO: raise if not found?
    downloaded = _downloaded()
    return [downloaded[int(mp3) - 1].name if mp3.isdigit() else mp3
            for mp3 in mp3s]


def escape_markdown(value):
    return value.replace('_', r'\_').replace('[', r'\[').replace('*', r'\*')
