from podcast_downloader import rss


def test_format_entry_minimal():
    entry = {'author': 'Podcast.__init__',
             'title': 'Infection Monkey',
             'pubDate': 'Tue, 04 Sep 2018 10:23:24 -0000',
             'url': 'https://podcasts.tompaton.com/downloaded/Episode-177-Infection-Monkey.mp3',
             'length': 12415914,
             'type': 'audio/mp3',
             'guid': 'Episode-177-Infection-Monkey.mp3',
             'description': '',
             'duration': '',
             'image': None}

    assert rss.format_entry(entry) == (
        '<item>\n'
        '        <author>Podcast.__init__</author>\n'
        '        <title>Infection Monkey</title>\n'
        '        <pubDate>Tue, 04 Sep 2018 10:23:24 -0000</pubDate>\n'
        '        <enclosure url="https://podcasts.tompaton.com/downloaded/Episode-177-Infection-Monkey.mp3" '
        'length="12415914" type="audio/mp3"/>\n'
        '        <guid>Episode-177-Infection-Monkey.mp3</guid>\n'
        '      </item>'
    )


def test_format_entry_full():
    entry = {'author': 'Podcast.__init__',
             'title': '"Infection Monkey"',
             'pubDate': 'Tue, 04 Sep 2018 10:23:24 -0000',
             'url': 'https://podcasts.tompaton.com/downloaded/Episode-177-Infection-Monkey.mp3',
             'length': 12415914,
             'type': 'audio/mp3',
             'guid': 'Episode-177-Infection-Monkey.mp3',
             'description': '<b>Part 1</b>',
             'duration': '01:02:03',
             'image': 'https://podcasts.tompaton.com/images/d194b9ac04b6b74bde672c6c16d6fcc8'}

    assert rss.format_entry(entry) == (
        '<item>\n'
        '        <author>Podcast.__init__</author>\n'
        '        <title>&quot;Infection Monkey&quot;</title>\n'
        '        <pubDate>Tue, 04 Sep 2018 10:23:24 -0000</pubDate>\n'
        '        <enclosure url="https://podcasts.tompaton.com/downloaded/Episode-177-Infection-Monkey.mp3" '
        'length="12415914" type="audio/mp3"/>\n'
        '        <guid>Episode-177-Infection-Monkey.mp3</guid>\n'
        '        <description><![CDATA[<b>Part 1</b>]]></description>\n'
        '        <itunes:duration>01:02:03</itunes:duration>\n'
        '        <itunes:image href="https://podcasts.tompaton.com/images/d194b9ac04b6b74bde672c6c16d6fcc8" />\n'
        '      </item>'
    )
